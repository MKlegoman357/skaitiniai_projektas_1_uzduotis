import math

import matplotlib.pyplot as plt

import roots
import utils


def f(x):
    """
    f(x)=-3x^2+8-sin(x)
    """
    return -3 * x ** 2 + 8 - math.sin(x)


def f_derivative(x):
    """
    f'(x)=-6x-cos(x)
    """
    return -6 * x - math.cos(x)


f.interval = (-20, 20)
f.root_intervals = roots.get_root_intervals(f, f.interval[0], f.interval[1], steps=25)

f.roots = {
    "skenavimo": roots.get_roots(f, f.root_intervals, roots.scan_root, precision=1e-10),
    "stygu": roots.get_roots(f, f.root_intervals, roots.get_root_chords, precision=1e-10),
    "kirstiniu": roots.get_roots(f, f.root_intervals, roots.get_root_secant, precision=1e-10),
    "niutono": roots.get_roots(f, f.root_intervals, roots.get_root_newton, f_derivative=f_derivative, precision=1e-10),
}

f.func = utils.Func(
    name="f(x)",
    f=f,
    x_from=f.interval[0],
    x_to=f.interval[1],
    root_intervals=f.root_intervals,
    y_scale=1
)

figs = [
    utils.display_f_plot(f.func, f.roots["skenavimo"], "skenavimo metodas", steps=1000),
    utils.display_f_plot(f.func, f.roots["stygu"], "stygų metodas", steps=1000),
    utils.display_f_plot(f.func, f.roots["kirstiniu"], "kirstinių (kvazi-Niutono) metodas", steps=1000),
    utils.display_f_plot(f.func, f.roots["niutono"], "Niutono metodas", steps=1000),
]

# wait for all figures to be closed
plt.show(block=True)
