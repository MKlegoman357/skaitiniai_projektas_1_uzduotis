import matplotlib.pyplot as plt
import numpy as np


class Func:
    def __init__(self, name, f, x_from, x_to, root_intervals, y_scale):
        self.name = name
        self.f = f
        self.range = (x_from, x_to)
        self.root_intervals = root_intervals
        self.y_scale = y_scale


def display_plot(f, x_from, x_to, y_scale, root_intervals=None, roots=None, step=None, steps=100, label=None,
                 title=None, plot_title=None):
    if step is None:
        step = (x_to - x_from) / steps

    if plot_title is None:
        plot_title = f"x = [{x_from}; {x_to}]"

    x = np.arange(x_from, x_to + step, step)
    y = np.vectorize(f)(x)

    y_span = (x_to - x_from) / 2. * y_scale

    fig, ax = plt.subplots(1, 1, num=title, dpi=100, figsize=(7, 5))

    # make the grid be drawn behind everything
    # this will make the grid's zorder=0.5, using plt.grid(..., zorder=0) does not work
    ax.set_axisbelow(True)

    plt.ylim(-y_span, y_span)
    plt.grid(True, color="0.8", lw=.5, aa=False)
    plt.axhline(color="0.2", lw=.5, aa=False)
    plt.title(plot_title)

    root_span = None
    if root_intervals is not None:
        for interval in root_intervals:
            root_span = plt.axvspan(interval[0], interval[1], edgecolor="#0000ff70", facecolor="#0000ff20")

    plot, = plt.plot(x, y)

    roots_plot = None

    if roots is not None:
        separated_coords = [*zip(*roots)]
        roots_x = separated_coords[0]
        roots_y = separated_coords[1]
        roots_plot = plt.scatter(roots_x, roots_y, color="#ff5555", zorder=3)

    plt.legend([plot, roots_plot, root_span], [label, "šaknys", "šaknų intervalai"])

    plt.tight_layout()

    return fig


def display_f_plot(f: Func, roots, subtitle, step=None, steps=100):
    print(f"{f.name} {subtitle}")
    print(f"{f.name} įvertis: {f.range[0]} - {f.range[1]}")
    print(f"{f.name} šaknų pradiniai intervalai: {f.root_intervals}")
    print(f"{f.name} šaknys: {roots}")
    print()

    fig = display_plot(
        f.f,
        f.range[0],
        f.range[1],
        f.y_scale,
        root_intervals=f.root_intervals,
        roots=roots,
        step=step,
        steps=steps,
        label=f.name,
        title=f"{f.name} - {subtitle}",
        plot_title=f"{subtitle}"
                   f"\nx = [{float(f.range[0]):.2g}; {float(f.range[1]):.2g}]"
                   f"\nšaknu intervalai:"
                   f"\n{', '.join([f'[{x_from:.3}; {x_to:.3}]' for x_from, x_to in f.root_intervals])}"
                   +
                   (f"\nšaknys (šaknis : iteracijos):"
                    f"\n{', '.join([f'{x:.6} : {i}' for x, y, i in roots])}" if roots is not None else f"")
    )

    return fig
