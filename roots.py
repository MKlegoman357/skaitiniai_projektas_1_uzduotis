import numpy as np


def get_roots(f, root_intervals, root_func, **kwargs):
    roots = []

    for root_interval in root_intervals:
        roots.append(root_func(f, root_interval[0], root_interval[1], **kwargs))

    return roots


def get_root_interval(f, x_from, x_to, step=None, steps=100):
    if step is None:
        step = (x_to - x_from) / steps

    prev_x = None
    prev_y = None
    i = 0
    for x in np.arange(x_from, x_to + step, step):
        y = f(x)

        if prev_x is not None and np.signbit(prev_y) != np.signbit(y):  # if the sign is different
            return prev_x, x, i

        prev_x = x
        prev_y = y

        i += 1

    return None, None, i


def get_root_intervals(f, x_from, x_to, step=None, steps=100):
    if step is None:
        step = (x_to - x_from) / steps

    roots = []

    while True:
        interval_from, interval_to, iterations = get_root_interval(f, x_from, x_to, step)

        if interval_from is None:
            return roots

        roots.append((interval_from, interval_to))
        x_from = interval_to


def scan_root(f, x_from, x_to, precision=1e-6, steps=2):
    x = (x_from + x_to) / 2.
    y = f(x)

    i = 0
    while abs(y) > precision:
        x_from, x_to, iterations = get_root_interval(f, x_from, x_to, steps=steps)

        if x_from is None:
            return x, y, i

        x = (x_from + x_to) / 2.
        y = f(x)

        i += iterations + 1

    return x, y, i


def get_root_chords(f, x_from, x_to, precision=1e-6):
    def calc_mid_point(y_from=None):
        y_from = f(x_from) if y_from is None else y_from
        k = abs(y_from / f(x_to))
        x_mid = (x_from + k * x_to) / (1 + k)
        return x_mid, f(x_mid), y_from

    x_mid, y_mid, y_from = calc_mid_point()

    i = 0
    while abs(y_mid) > precision:
        if np.signbit(y_mid) == np.signbit(y_from):
            x_from = x_mid
            y_from = y_mid
        else:
            x_to = x_mid

        x_mid, y_mid, y_from = calc_mid_point(y_from)

        i += 1

    return x_mid, y_mid, i


def get_root_secant(f, x_from, x_to, precision=1e-6):
    x_prev, x = x_to, (x_to + x_from) / 2
    prev_y, y = f(x_prev), f(x)

    i = 0
    while abs(y) > precision:
        x_prev, x = x, x - (x - x_prev) / (y - prev_y) * y
        prev_y, y = y, f(x)
        i += 1

    return x, y, i


def get_root_newton(f, x_from, x_to, f_derivative, precision=1e-6):
    x = (x_to + x_from) / 2
    y = f(x)

    i = 0
    while abs(y) > precision:
        x = x - 1 / f_derivative(x) * y
        y = f(x)
        i += 1

    return x, y, i
