import math

import matplotlib.pyplot as plt

import roots
import utils

g = 9.8
m = 60
t = 4
v = 30


def f(c):
    """
    v(t)=mg/c * (1 - e^-(c/m)t)
    """
    return m * g / c * (1 - math.e ** -(c / m * t)) - v


f.interval = (1, 10)
f.root_intervals = roots.get_root_intervals(f, f.interval[0], f.interval[1], steps=10)

f.roots = {
    "secant": roots.get_roots(f, f.root_intervals, roots.get_root_secant, precision=1e-10)
}

f.func = utils.Func(
    name="f(c)",
    f=f,
    x_from=f.interval[0],
    x_to=f.interval[1],
    root_intervals=f.root_intervals,
    y_scale=1
)

utils.display_f_plot(f.func, f.roots["secant"], "kvazi-Niutono metodu", steps=1000)

# wait for all figures to be closed
plt.show(block=True)
