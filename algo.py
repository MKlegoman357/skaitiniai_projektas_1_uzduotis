import numpy as np


def grubus_ivertis(a):
    if a[0] > 0:
        return 1.0 + max(np.abs(a[1:])) / a[0]
    else:
        raise ValueError("the first number must be a positive number")


def tikslus_ivertis_r(a):
    b = max([abs(n) for n in a[1:] if n < 0], default=0.0)
    k = len(a) - 1 - max([len(a) - i - 2 for i, n in enumerate(a[1:]) if n < 0], default=0)
    r = 1.0 + ((b / a[0]) ** (1.0 / k))
    return r


def tikslus_ivertis(a):
    if a[0] < 0:
        a = [-n for n in a]

    grubus = grubus_ivertis(a)
    r_positive = tikslus_ivertis_r(a)
    if (len(a) - 1) % 2 == 0:
        r_negative = tikslus_ivertis_r([n if i % 2 != 0 else -n for i, n in enumerate(a)])
    else:
        r_negative = tikslus_ivertis_r([-n if i % 2 != 0 else n for i, n in enumerate(a)])
    return -min(grubus, r_negative), min(grubus, r_positive)
