import math

import matplotlib.pyplot as plt

import algo
import utils


def f(x):
    """
    f(x)=0.25x^5+0.68x^4-1.65x^3-5.26x^2-1.91x+1.36
    """
    return 0.25 * (x ** 5) + 0.68 * (x ** 4) - 1.65 * (x ** 3) - 5.26 * (x ** 2) - 1.91 * x + 1.36


f.coefficients = [0.25, 0.68, -1.65, -5.26, -1.91, 1.36]
f.grubus_interval = (-algo.grubus_ivertis(f.coefficients), algo.grubus_ivertis(f.coefficients))
f.interval = algo.tikslus_ivertis(f.coefficients)

f.func = utils.Func(
    name="f(x)",
    f=f,
    x_from=f.interval[0],
    x_to=f.interval[1],
    root_intervals=[],
    y_scale=1
)

f.func_grubus = utils.Func(
    name="f(x)",
    f=f,
    x_from=f.grubus_interval[0],
    x_to=f.grubus_interval[1],
    root_intervals=[],
    y_scale=1
)


def g(x):
    """
    g(x)=e^-x * cos(x) * sin(x^2 - 1)
    """
    return (math.e ** -x) * math.cos(x) * math.sin(x ** 2 - 1)


g.interval = (7, 8)

g.func = utils.Func(
    name="g(x)",
    f=g,
    x_from=g.interval[0],
    x_to=g.interval[1],
    root_intervals=[],
    y_scale=.0001
)

utils.display_plot(f, f.func.range[0], f.func.range[1], f.func.y_scale, [(f.func.range[0], f.func.range[1])], steps=1000, label=f.func.name)
utils.display_plot(f, f.func_grubus.range[0], f.func_grubus.range[1], f.func_grubus.y_scale, [(f.func_grubus.range[0], f.func_grubus.range[1])], steps=1000, label=f.func.name)

utils.display_plot(g, g.func.range[0], g.func.range[1], g.func.y_scale, [(g.func.range[0], g.func.range[1])], steps=1000, label=g.func.name)

plt.show(block=True)
