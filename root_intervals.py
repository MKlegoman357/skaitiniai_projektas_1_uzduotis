import math

import matplotlib.pyplot as plt

import algo
import roots
import utils


def f(x):
    """
    f(x)=0.25x^5+0.68x^4-1.65x^3-5.26x^2-1.91x+1.36
    """
    return 0.25 * (x ** 5) + 0.68 * (x ** 4) - 1.65 * (x ** 3) - 5.26 * (x ** 2) - 1.91 * x + 1.36


f.coefficients = [0.25, 0.68, -1.65, -5.26, -1.91, 1.36]
f.interval = algo.tikslus_ivertis(f.coefficients)
f.root_intervals = roots.get_root_intervals(f, f.interval[0], f.interval[1], steps=30)

f.func = utils.Func(
    name="f(x)",
    f=f,
    x_from=f.interval[0],
    x_to=f.interval[1],
    root_intervals=f.root_intervals,
    y_scale=1
)


def g(x):
    """
    g(x)=e^-x * cos(x) * sin(x^2 - 1)
    """
    return (math.e ** -x) * math.cos(x) * math.sin(x ** 2 - 1)


g.interval = (7, 8)
g.root_intervals = roots.get_root_intervals(g, g.interval[0], g.interval[1], steps=20)

g.func = utils.Func(
    name="g(x)",
    f=g,
    x_from=g.interval[0],
    x_to=g.interval[1],
    root_intervals=g.root_intervals,
    y_scale=.0001
)

utils.display_f_plot(f.func, None, "šaknų intervalai", steps=1000)
utils.display_f_plot(g.func, None, "šaknų intervalai", steps=1000)

# wait for all figures to be closed
plt.show(block=True)
