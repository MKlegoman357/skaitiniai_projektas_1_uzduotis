import math

import matplotlib.pyplot as plt

import algo
import roots
import utils


def f(x):
    """
    f(x)=0.25x^5+0.68x^4-1.65x^3-5.26x^2-1.91x+1.36
    """
    return 0.25 * (x ** 5) + 0.68 * (x ** 4) - 1.65 * (x ** 3) - 5.26 * (x ** 2) - 1.91 * x + 1.36


f.coefficients = [0.25, 0.68, -1.65, -5.26, -1.91, 1.36]
f.interval = algo.tikslus_ivertis(f.coefficients)
f.root_intervals = roots.get_root_intervals(f, f.interval[0], f.interval[1], steps=30)

f.roots = {
    "skenavimo": roots.get_roots(f, f.root_intervals, roots.scan_root, precision=1e-10),
    "stygu": roots.get_roots(f, f.root_intervals, roots.get_root_chords, precision=1e-10),
    "kirstiniu": roots.get_roots(f, f.root_intervals, roots.get_root_secant, precision=1e-10),
}

f.func = utils.Func(
    name="f(x)",
    f=f,
    x_from=f.interval[0],
    x_to=f.interval[1],
    root_intervals=f.root_intervals,
    y_scale=1
)


def g(x):
    """
    g(x)=e^-x * cos(x) * sin(x^2 - 1)
    """
    return (math.e ** -x) * math.cos(x) * math.sin(x ** 2 - 1)


g.interval = (7, 8)
g.root_intervals = roots.get_root_intervals(g, g.interval[0], g.interval[1], steps=20)

g.roots = {
    "skenavimo": roots.get_roots(g, g.root_intervals, roots.scan_root, precision=1e-10),
    "stygu": roots.get_roots(g, g.root_intervals, roots.get_root_chords, precision=1e-10),
    "kirstiniu": roots.get_roots(g, g.root_intervals, roots.get_root_secant, precision=1e-10),
}

g.func = utils.Func(
    name="g(x)",
    f=g,
    x_from=g.interval[0],
    x_to=g.interval[1],
    root_intervals=g.root_intervals,
    y_scale=.0001
)

figs = [
    utils.display_f_plot(f.func, f.roots["skenavimo"], "skenavimo metodas", steps=1000),
    utils.display_f_plot(f.func, f.roots["stygu"], "stygų metodas", steps=1000),
    utils.display_f_plot(f.func, f.roots["kirstiniu"], "kirstinių (kvazi-Niutono) metodas", steps=1000),

    utils.display_f_plot(g.func, g.roots["skenavimo"], "skenavimo metodas", steps=1000),
    utils.display_f_plot(g.func, g.roots["stygu"], "stygų metodas", steps=1000),
    utils.display_f_plot(g.func, g.roots["kirstiniu"], "kirstinių (kvazi-Niutono) metodas", steps=1000),
]

# wait for all figures to be closed
plt.show(block=True)
